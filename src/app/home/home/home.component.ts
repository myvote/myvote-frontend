import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { PartyService } from '@core/services/party.service';
import { Party } from '@core/models/party.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  parties$: Observable<Array<Party>>;

  constructor(private partyService: PartyService) {}

  ngOnInit() {
    this.parties$ = this.partyService.getAll();
  }
}
