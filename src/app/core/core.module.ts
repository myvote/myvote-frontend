import { CityEffects } from './store/effects/city.effects';
import { PartyEffects } from './store/effects/party.effects';
import { AuthService } from './services/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { partyReducer } from '@core/store/reducers/party.reducer';
import { EffectsModule } from '@ngrx/effects';
import { cityReducer } from '@core/store/reducers/city.reducer';
import { userReducer } from './store/reducers/user.reducer';
import { UserEffects } from './store/effects/user.effects';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot({ users: userReducer, parties: partyReducer, cities: cityReducer }),
    StoreDevtoolsModule.instrument({}),
    EffectsModule.forRoot([UserEffects, PartyEffects, CityEffects])
  ],
  declarations: []
})
export class CoreModule {}
