import { PartyRequest } from './../models/requests/party.request.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Party } from '@core/models/party.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartyService {
  url = 'http://localhost:8000/';
  constructor(private httpClient: HttpClient) {}

  add(party: PartyRequest): Observable<Party> {
    return this.httpClient.post<Party>(this.url + 'parties', party.getPostBody());
  }

  getAll(): Observable<Array<Party>> {
    return this.httpClient.get<Array<Party>>(this.url + 'parties');
  }

  update(party: PartyRequest): Observable<Party> {
    return this.httpClient.put<Party>(this.url + 'parties/' + party.id, party.getPutBody());
  }

  delete(party: PartyRequest): Observable<Party> {
    return this.httpClient.delete<Party>(this.url + `parties/${party.id}`);
  }
}
