import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CityRequest } from '@core/models/requests/city.request.model';
import { Observable } from 'rxjs';
import { City } from '@core/models/city.model';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  url = 'http://localhost:8000/';

  constructor(private httpClient: HttpClient) {}

  add(city: CityRequest): Observable<City> {
    return this.httpClient.post<City>(this.url + 'cities', city.getPostBody());
  }

  getAll(): Observable<Array<City>> {
    return this.httpClient.get<Array<City>>(this.url + 'cities');
  }

  update(city: CityRequest): Observable<City> {
    return this.httpClient.put<City>(this.url + 'cities/' + city.id, city.getPutBody());
  }

  delete(city: CityRequest): Observable<City> {
    return this.httpClient.delete<City>(this.url + `cities/${city.id}`);
  }
}
