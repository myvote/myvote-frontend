import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '@core/models/user.model';
import { UserRequest } from '@core/models/requests/user.request.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'http://localhost:8000/';
  constructor(private http: HttpClient) {}

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.url + 'users');
  }

  get(id: string): Observable<User> {
    return this.http.get<User>(this.url + `users/${id}`);
  }

  add(user: UserRequest): Observable<User> {
    return this.http.post<User>(this.url + `users/${user.id}`, user.getPostBody());
  }

  update(user: UserRequest): Observable<User> {
    return this.http.put<User>(this.url + `users/${user.id}`, user.getPutBody());
  }
  delete(user: UserRequest): Observable<User> {
    return this.http.delete<User>(this.url + `users/${user.id}`);
  }
}
