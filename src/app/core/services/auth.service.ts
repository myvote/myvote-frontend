import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserRequest } from '@core/models/requests/user.request.model';
import { Observable } from 'rxjs';
import { UserLoginRequest } from '@core/models/requests/user-login.request.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = 'http://localhost:8000/';
  private jwt = new JwtHelperService();

  constructor(private httpClient: HttpClient) {}

  isAdmin(): boolean {
    return this.getToken() && this.jwt.decodeToken(this.getToken())['admin'];
  }

  isParty(): boolean {
    return this.isAdmin();
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  clear() {
    localStorage.removeItem('token');
  }

  login(user: UserLoginRequest): Observable<any> {
    return this.httpClient.post(this.url + 'login', user);
  }
}
