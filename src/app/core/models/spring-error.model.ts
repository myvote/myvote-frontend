export interface SpringError {
  error: string;
  message: string;
  timestamp: Date;
  status: number;
  path: string;
}
