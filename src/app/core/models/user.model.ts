import { Party } from '@core/models/party.model';

export class User {
  constructor(
    public id: string = '',
    public firstname: string = '',
    public lastname: string = '',
    public admin: boolean = false,
    public party: Party = null,
    public email: string = null
  ) {}
}
