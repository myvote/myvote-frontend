import { City } from '@core/models/city.model';

export class Party {
  id: string;
  name: string;
  city: City;
  parent: Party;
}
