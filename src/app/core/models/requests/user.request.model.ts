import { User } from '@core/models/user.model';
import { Party } from '@core/models/party.model';
import { Utils } from '@shared/utils';

export class UserRequest {
  constructor(
    public id: string = '',
    public firstname: string = '',
    public lastname: string = '',
    public admin: boolean = false,
    public partyId: string = null,
    public email: string = null
  ) {}

  static from(user: User) {
    return new UserRequest(
      user.id,
      user.firstname,
      user.lastname,
      user.admin,
      user.party ? user.party.id : null,
      user.email
    );
  }

  validate(): string[] {
    const out = [];

    if (this.firstname == null || this.firstname.length < 2) {
      out.push('Firstname should be at least 5 characters');
    }

    if (this.lastname == null || this.lastname.length < 2) {
      out.push('Lastname should be at least 5 characters');
    }

    if (this.email == null || this.email.length <= 0) {
      out.push('Email is required');
    } else if (!new RegExp(Utils.emailRegex).test(this.email)) {
      out.push('Please provide a valid email.');
    }

    return out;
  }

  getPostBody(): Object {
    return {
      firstname: this.firstname,
      lastname: this.lastname,
      admin: this.admin,
      party_id: this.partyId,
      email: this.email
    };
  }

  getPutBody(): Object {
    return this.getPostBody();
  }
}
