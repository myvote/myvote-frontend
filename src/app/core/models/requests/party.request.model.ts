import { Party } from '@core/models/party.model';

export class PartyRequest {
  constructor(
    public id: string = '',
    public name: string = '',
    public cityId: string = '',
    public parentId: string = null
  ) {}

  static from(party: Party) {
    return new PartyRequest(party.id, party.name, party.city.id, party.parent ? party.parent.id : null);
  }

  validate(): string[] {
    const fdb = [];

    if (this.name.length < 3) {
      fdb.push('Name should be at least 3 characters.');
    }
    if (this.cityId.length <= 0) {
      fdb.push('City should be given.');
    }

    return fdb;
  }

  getPostBody(): Object {
    return {
      name: this.name,
      city_id: this.cityId,
      parent_id: this.parentId
    };
  }

  getPutBody(): Object {
    return this.getPostBody();
  }
}
