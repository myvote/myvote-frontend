import { Utils } from '@shared/utils';

export class UserLoginRequest {
  constructor(public email: string = '', public password: string = '') {}

  validate(): string[] {
    const out = [];

    if (this.email == null || this.email.length <= 0) {
      out.push('Email is required.');
    } else if (!new RegExp(Utils.emailRegex).test(this.email)) {
      out.push('Email is not valid');
    }

    if (this.password == null || this.password.length < 3) {
      out.push('Password is required');
    }

    return out;
  }
}
