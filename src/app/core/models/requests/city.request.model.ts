import { City } from '@core/models/city.model';

export class CityRequest {
  constructor(public id: String, public name: String) {}

  static from(city: City) {
    return new CityRequest(city.id, city.name);
  }

  isValid(): string[] {
    const fb = [];

    if (this.name.length < 5) {
      fb.push('City names should be at least 5 characters');
    }

    return fb;
  }

  getPostBody() {
    return this.getPutBody();
  }

  getPutBody() {
    return {
      name: this.name
    };
  }
}
