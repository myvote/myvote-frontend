import { Injectable } from '@angular/core';
import { UserService } from '@core/services/user.service';
import { UserAddSuccessAction, UserLoadFailAction, UserLoadSuccessAction } from '@core/store/actions/user.actions';
import { user } from '@core/store/types/user.types';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import {
  UserAddFailAction,
  UserDeleteFailAction,
  UserDeleteSuccessAction,
  UserUpdateFailAction,
  UserUpdateSuccessAction
} from '../actions/user.actions';

@Injectable()
export class UserEffects {
  constructor(private actions$: Actions, private userService: UserService) {}

  @Effect()
  addUser$: Observable<Action> = this.actions$.pipe(
    ofType(user.ADD),
    mergeMap(action =>
      this.userService.add(action['payload']).pipe(
        map(res => new UserAddSuccessAction(res)),
        catchError(res => of(new UserAddFailAction(res.error)))
      )
    )
  );

  @Effect()
  loadUsers$: Observable<Action> = this.actions$.pipe(
    ofType(user.LOAD),
    switchMap(() =>
      this.userService.getAll().pipe(
        map(res => new UserLoadSuccessAction(res)),
        catchError(res => of(new UserLoadFailAction(res.error)))
      )
    )
  );

  @Effect()
  updateUser$: Observable<Action> = this.actions$.pipe(
    ofType(user.UPDATE),
    mergeMap(action =>
      this.userService.update(action['payload']).pipe(
        map(res => new UserUpdateSuccessAction(res)),
        catchError(res => of(new UserUpdateFailAction(res.error)))
      )
    )
  );

  @Effect()
  deleteUser$: Observable<Action> = this.actions$.pipe(
    ofType(user.DELETE),
    mergeMap(action =>
      this.userService.delete(action['payload']).pipe(
        map(res => new UserDeleteSuccessAction(res)),
        catchError(res => of(new UserDeleteFailAction(res.error)))
      )
    )
  );
}
