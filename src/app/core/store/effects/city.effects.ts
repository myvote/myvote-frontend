import {
  CityAddSuccessAction,
  CityAddFailAction,
  CityUpdateFailAction,
  CityUpdateSuccessAction,
  CityDeleteSuccessAction,
  CityDeleteFailAction
} from './../actions/city.actions';
import { Injectable } from '@angular/core';
import { PartyService } from '@core/services/party.service';
import { CityLoadFailAction, CityLoadSuccessAction } from '@core/store/actions/city.actions';
import { city } from '@core/store/types/city.types';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, mergeMap, tap } from 'rxjs/operators';
import { CityService } from '@core/services/city.service';

@Injectable()
export class CityEffects {
  constructor(private actions$: Actions, private cityService: CityService) {}

  @Effect()
  addCity$: Observable<Action> = this.actions$.pipe(
    ofType(city.ADD),
    mergeMap(action =>
      this.cityService.add(action['payload']).pipe(
        map(res => new CityAddSuccessAction(res)),
        catchError(res => of(new CityAddFailAction(res.error)))
      )
    )
  );

  @Effect()
  loadCities$: Observable<Action> = this.actions$.pipe(
    ofType(city.LOAD),
    switchMap(() =>
      this.cityService.getAll().pipe(
        map(res => new CityLoadSuccessAction(res)),
        catchError(res => of(new CityLoadFailAction(res.error)))
      )
    )
  );

  @Effect()
  updateCity$: Observable<Action> = this.actions$.pipe(
    ofType(city.UPDATE),
    mergeMap(action =>
      this.cityService.update(action['payload']).pipe(
        map(res => new CityUpdateSuccessAction(res)),
        catchError(res => of(new CityUpdateFailAction(res.error)))
      )
    )
  );

  @Effect()
  deleteCity$: Observable<Action> = this.actions$.pipe(
    ofType(city.DELETE),
    mergeMap(action =>
      this.cityService.delete(action['payload']).pipe(
        map(res => new CityDeleteSuccessAction(res)),
        catchError(res => of(new CityDeleteFailAction(res.error)))
      )
    )
  );
}
