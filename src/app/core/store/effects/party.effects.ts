import { Injectable } from '@angular/core';
import { PartyService } from '@core/services/party.service';
import { PartyAddSuccessAction, PartyLoadFailAction, PartyLoadSuccessAction } from '@core/store/actions/party.actions';
import { party } from '@core/store/types/party.types';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import {
  PartyAddFailAction,
  PartyDeleteFailAction,
  PartyDeleteSuccessAction,
  PartyUpdateFailAction,
  PartyUpdateSuccessAction
} from './../actions/party.actions';

@Injectable()
export class PartyEffects {
  constructor(private actions$: Actions, private partyService: PartyService) {}

  @Effect()
  addParty$: Observable<Action> = this.actions$.pipe(
    ofType(party.ADD),
    mergeMap(action =>
      this.partyService.add(action['payload']).pipe(
        map(res => new PartyAddSuccessAction(res)),
        catchError(res => of(new PartyAddFailAction(res.error)))
      )
    )
  );

  @Effect()
  loadParties$: Observable<Action> = this.actions$.pipe(
    ofType(party.LOAD),
    switchMap(() =>
      this.partyService.getAll().pipe(
        map(res => new PartyLoadSuccessAction(res)),
        catchError(res => of(new PartyLoadFailAction(res.error)))
      )
    )
  );

  @Effect()
  updateParty$: Observable<Action> = this.actions$.pipe(
    ofType(party.UPDATE),
    mergeMap(action =>
      this.partyService.update(action['payload']).pipe(
        map(res => new PartyUpdateSuccessAction(res)),
        catchError(res => of(new PartyUpdateFailAction(res.error)))
      )
    )
  );

  @Effect()
  deleteParty$: Observable<Action> = this.actions$.pipe(
    ofType(party.DELETE),
    mergeMap(action =>
      this.partyService.delete(action['payload']).pipe(
        map(res => new PartyDeleteSuccessAction(res)),
        catchError(res => of(new PartyDeleteFailAction(res.error)))
      )
    )
  );
}
