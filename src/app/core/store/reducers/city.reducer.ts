import * as CityActions from '@core/store/actions/city.actions';
import { Party } from '@core/models/party.model';
import { city } from '@core/store/types/city.types';
import { City } from '@core/models/city.model';

export function cityReducer(state: City[] = [], action: CityActions.ALL) {
  switch (action.type) {
    // ADD
    case city.ADD_SUCCESS:
      return [...state, action['payload']];

    // LOAD
    case city.LOAD_SUCCESS:
      return action['payload'];

    // UPDATE
    case city.UPDATE_SUCCESS:
      return state.map(c => {
        if (c.id === action['payload']['id']) {
          return action['payload'];
        } else {
          return c;
        }
      });

    // DELETE
    case city.DELETE_SUCCESS:
      return state.filter(c => c.id !== action['payload'].id);

    default:
      return state;
  }
}
