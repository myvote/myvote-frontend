import * as UserActions from '@core/store/actions/user.actions';
import { User } from '@core/models/user.model';
import { user } from '@core/store/types/user.types';
import { createSelector } from '@ngrx/store';

const getUsers = state => state.users;
export const getUserById = (id: String) =>
  createSelector(getUsers, us => {
    const coll = us.filter(u => u.id === id);
    if (coll.length > 0) {
      return coll[0];
    }
    return new User();
  });

export function userReducer(state: User[] = [], action: UserActions.ALL) {
  switch (action.type) {
    // ADD
    case user.ADD_SUCCESS:
      return [...state, action['payload']];

    // LOAD
    case user.LOAD_SUCCESS:
      return action['payload'];

    // UPDATE
    case user.UPDATE_SUCCESS:
      return state.map(p => {
        if (p.id === action['payload']['id']) {
          return action['payload'];
        } else {
          return p;
        }
      });

    // DELETE
    case user.DELETE_SUCCESS:
      return state.filter(p => p.id !== action['payload'].id);
    default:
      return state;
  }
}
