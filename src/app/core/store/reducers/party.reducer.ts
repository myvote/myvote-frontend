import * as PartyActions from '@core/store/actions/party.actions';
import { Party } from '@core/models/party.model';
import { party } from '@core/store/types/party.types';
import { createSelector } from '@ngrx/store';

const getParties = state => state.parties;
export const getPartiesExclude = (id: String) =>
  createSelector(getParties, parties => parties.filter(p => p.id !== id));

export function partyReducer(state: Party[] = [], action: PartyActions.ALL) {
  switch (action.type) {
    // ADD
    case party.ADD_SUCCESS:
      return [...state, action['payload']];

    // LOAD
    case party.LOAD_SUCCESS:
      return action['payload'];

    // UPDATE
    case party.UPDATE_SUCCESS:
      return state.map(p => {
        if (p.id === action['payload']['id']) {
          return action['payload'];
        } else {
          return p;
        }
      });

    // DELETE
    case party.DELETE_SUCCESS:
      return state.filter(p => p.id !== action['payload'].id);
    default:
      return state;
  }
}
