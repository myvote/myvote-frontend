export const city = {
  ADD: '[CITY] add',
  ADD_SUCCESS: '[CITY] add - success',
  ADD_FAIL: '[CITY] add - fail',

  LOAD: '[CITY] load',
  LOAD_SUCCESS: '[CITY] load - success',
  LOAD_FAIL: '[CITY] load - fail',

  UPDATE: '[CITY] update',
  UPDATE_SUCCESS: '[CITY] update - success',
  UPDATE_FAIL: '[CITY] update - fail',

  DELETE: '[CITY] delete',
  DELETE_SUCCESS: '[CITY] delete - success',
  DELETE_FAIL: '[CITY] delete - fail'
};
