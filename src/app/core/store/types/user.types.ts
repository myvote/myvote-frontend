export const user = {
  ADD: '[USER] add',
  ADD_SUCCESS: '[USER] add - success',
  ADD_FAIL: '[USER] add - fail',

  LOAD: '[USER] load',
  LOAD_SUCCESS: '[USER] load - success',
  LOAD_FAIL: '[USER] load - fail',

  UPDATE: '[USER] update',
  UPDATE_SUCCESS: '[USER] update - success',
  UPDATE_FAIL: '[USER] update - fail',

  DELETE: '[USER] delete',
  DELETE_SUCCESS: '[USER] delete - success',
  DELETE_FAIL: '[USER] delete - fail'
};
