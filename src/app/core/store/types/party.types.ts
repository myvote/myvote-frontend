export const party = {
  ADD: '[PARTY] add',
  ADD_SUCCESS: '[PARTY] add - success',
  ADD_FAIL: '[PARTY] add - fail',

  LOAD: '[PARTY] load',
  LOAD_SUCCESS: '[PARTY] load - success',
  LOAD_FAIL: '[PARTY] load - fail',

  UPDATE: '[PARTY] update',
  UPDATE_SUCCESS: '[PARTY] update - success',
  UPDATE_FAIL: '[PARTY] update - fail',

  DELETE: '[PARTY] delete',
  DELETE_SUCCESS: '[PARTY] delete - success',
  DELETE_FAIL: '[PARTY] delete - fail'
};
