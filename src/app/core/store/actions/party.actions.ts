import { SpringError } from '@core/models/spring-error.model';
import { Party } from '@core/models/party.model';
import { party } from '@core/store/types/party.types';
import { Action } from '@ngrx/store';
import { PartyRequest } from './../../models/requests/party.request.model';

/**
 * ADD
 */
export class PartyAddAction implements Action {
  readonly type = party.ADD;

  constructor(public payload: PartyRequest) {}
}

export class PartyAddSuccessAction implements Action {
  readonly type = party.ADD_SUCCESS;

  constructor(public payload: Party) {}
}

export class PartyAddFailAction implements Action {
  readonly type = party.ADD_FAIL;

  constructor(public payload: SpringError) {}
}

/**
 * LOAD
 */
export class PartyLoadAction implements Action {
  readonly type = party.LOAD;

  constructor() {}
}

export class PartyLoadSuccessAction implements Action {
  readonly type = party.LOAD_SUCCESS;

  constructor(private payload: Party[]) {}
}

export class PartyLoadFailAction implements Action {
  readonly type = party.LOAD_FAIL;

  constructor(private payload: SpringError) {}
}

/**
 * UPDATE
 */
export class PartyUpdateAction implements Action {
  readonly type = party.UPDATE;

  constructor(public payload: PartyRequest) {}
}

export class PartyUpdateSuccessAction implements Action {
  readonly type = party.UPDATE_SUCCESS;

  constructor(public payload: Party) {}
}

export class PartyUpdateFailAction implements Action {
  readonly type = party.UPDATE_FAIL;

  constructor(public payload: SpringError) {}
}

/**
 * DELETE
 */
export class PartyDeleteAction implements Action {
  readonly type = party.DELETE;

  constructor(public payload: PartyRequest) {}
}

export class PartyDeleteSuccessAction implements Action {
  readonly type = party.DELETE_SUCCESS;

  constructor(public payload: Party) {}
}

export class PartyDeleteFailAction implements Action {
  readonly type = party.DELETE_FAIL;

  constructor(public payload: SpringError) {}
}

export type ALL =
  | PartyLoadAction
  | PartyLoadSuccessAction
  | PartyLoadFailAction
  | PartyAddAction
  | PartyAddSuccessAction
  | PartyAddFailAction
  | PartyUpdateAction
  | PartyUpdateSuccessAction
  | PartyUpdateFailAction
  | PartyDeleteAction
  | PartyDeleteSuccessAction
  | PartyDeleteFailAction;
