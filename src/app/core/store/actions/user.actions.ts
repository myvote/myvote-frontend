import { SpringError } from '@core/models/spring-error.model';
import { User } from '@core/models/user.model';
import { user } from '@core/store/types/user.types';
import { Action } from '@ngrx/store';
import { UserRequest } from '../../models/requests/user.request.model';

/**
 * ADD
 */
export class UserAddAction implements Action {
  readonly type = user.ADD;

  constructor(public payload: UserRequest) {}
}

export class UserAddSuccessAction implements Action {
  readonly type = user.ADD_SUCCESS;

  constructor(public payload: User) {}
}

export class UserAddFailAction implements Action {
  readonly type = user.ADD_FAIL;

  constructor(public payload: SpringError) {}
}

/**
 * LOAD
 */
export class UserLoadAction implements Action {
  readonly type = user.LOAD;

  constructor() {}
}

export class UserLoadSuccessAction implements Action {
  readonly type = user.LOAD_SUCCESS;

  constructor(private payload: User[]) {}
}

export class UserLoadFailAction implements Action {
  readonly type = user.LOAD_FAIL;

  constructor(private payload: SpringError) {}
}

/**
 * UPDATE
 */
export class UserUpdateAction implements Action {
  readonly type = user.UPDATE;

  constructor(public payload: UserRequest) {}
}

export class UserUpdateSuccessAction implements Action {
  readonly type = user.UPDATE_SUCCESS;

  constructor(public payload: User) {}
}

export class UserUpdateFailAction implements Action {
  readonly type = user.UPDATE_FAIL;

  constructor(public payload: SpringError) {}
}

/**
 * DELETE
 */
export class UserDeleteAction implements Action {
  readonly type = user.DELETE;

  constructor(public payload: UserRequest) {}
}

export class UserDeleteSuccessAction implements Action {
  readonly type = user.DELETE_SUCCESS;

  constructor(public payload: User) {}
}

export class UserDeleteFailAction implements Action {
  readonly type = user.DELETE_FAIL;

  constructor(public payload: SpringError) {}
}

export type ALL =
  | UserLoadAction
  | UserLoadSuccessAction
  | UserLoadFailAction
  | UserAddAction
  | UserAddSuccessAction
  | UserAddFailAction
  | UserUpdateAction
  | UserUpdateSuccessAction
  | UserUpdateFailAction
  | UserDeleteAction
  | UserDeleteSuccessAction
  | UserDeleteFailAction;
