import { CityRequest } from './../../models/requests/city.request.model';
import { city } from '@core/store/types/city.types';
import { Action } from '@ngrx/store';
import { City } from './../../models/city.model';
import { SpringError } from '@core/models/spring-error.model';

/**
 * ADD
 */
export class CityAddAction implements Action {
  readonly type = city.ADD;

  constructor(public payload: CityRequest) {}
}
export class CityAddSuccessAction implements Action {
  readonly type = city.ADD_SUCCESS;

  constructor(public payload: City) {}
}
export class CityAddFailAction implements Action {
  readonly type = city.ADD_FAIL;

  constructor(public payload: SpringError) {}
}
/**
 * LOAD
 */
export class CityLoadAction implements Action {
  readonly type = city.LOAD;

  constructor() {}
}

export class CityLoadSuccessAction implements Action {
  readonly type = city.LOAD_SUCCESS;

  constructor(public payload: City[]) {}
}

export class CityLoadFailAction implements Action {
  readonly type = city.LOAD_FAIL;

  constructor(public payload: SpringError) {}
}

/**
 * UPDATE
 */
export class CityUpdateAction implements Action {
  readonly type = city.UPDATE;

  constructor(public payload: CityRequest) {}
}

export class CityUpdateSuccessAction implements Action {
  readonly type = city.UPDATE_SUCCESS;

  constructor(public payload: City) {}
}

export class CityUpdateFailAction implements Action {
  readonly type = city.UPDATE_FAIL;

  constructor(public payload: SpringError) {}
}

export class CityDeleteAction implements Action {
  readonly type = city.DELETE;

  constructor(public payload: CityRequest) {}
}

export class CityDeleteSuccessAction implements Action {
  readonly type = city.DELETE_SUCCESS;

  constructor(public payload: City) {}
}

export class CityDeleteFailAction implements Action {
  readonly type = city.DELETE_FAIL;

  constructor(public payload: SpringError) {}
}

export type ALL =
  | CityLoadAction
  | CityLoadSuccessAction
  | CityLoadFailAction
  | CityAddAction
  | CityAddSuccessAction
  | CityAddFailAction
  | CityUpdateAction
  | CityUpdateSuccessAction
  | CityUpdateFailAction
  | CityDeleteAction
  | CityDeleteSuccessAction
  | CityDeleteFailAction;
