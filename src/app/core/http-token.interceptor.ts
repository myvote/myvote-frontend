import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs';
import { HttpInterceptor } from '@angular/common/http/src/interceptor';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any> | any> {
    const token = this.authService.getToken();
    if (token) {
      req = req.clone({ headers: req.headers.set('Authorization', `Bearer ${token}`) });
    }

    return next.handle(req).pipe(
      tap(
        event => {},
        err => {
          if (err instanceof HttpErrorResponse && (err.status === 401 || err.status === 403)) {
            this.authService.clear();
            this.router.navigateByUrl('/login');
          }
        }
      )
    );
  }
}
