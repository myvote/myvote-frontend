import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from './../shared/shared.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from './../core/core.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { CityComponent } from './city/city.component';
import { HomeComponent } from './home/home.component';
import { PartyComponent } from './party/party.component';
import { PartyDetailComponent } from './party/party-detail/party-detail.component';
import { FormsModule } from '@angular/forms';
import { CityDetailComponent } from './city/city-detail/city-detail.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgbPaginationModule,
    FormsModule,
    NgSelectModule,
    SharedModule,
    CoreModule
  ],
  declarations: [
    AdminComponent,
    HomeComponent,
    CityComponent,
    PartyComponent,
    PartyDetailComponent,
    CityDetailComponent,
    UsersComponent,
    UserDetailComponent
  ]
})
export class AdminModule {}
