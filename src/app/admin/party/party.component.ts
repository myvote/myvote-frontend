import { Component, OnInit } from '@angular/core';
import { Party } from '@core/models/party.model';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from './../../app.state';

@Component({
  selector: 'app-party',
  templateUrl: './party.component.html',
  styleUrls: ['./party.component.scss']
})
export class PartyComponent implements OnInit {
  parties$: Observable<Array<Party>>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.parties$ = this.store.select(state => state.parties);
  }
}
