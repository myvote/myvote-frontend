import { getPartiesExclude } from './../../../core/store/reducers/party.reducer';
import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { City } from '@core/models/city.model';
import { Party } from '@core/models/party.model';
import { PartyAddAction, PartyUpdateAction, PartyDeleteAction } from '@core/store/actions/party.actions';
import { party as partyTypes } from '@core/store/types/party.types';
import { Actions, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { from, Observable, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { AppState } from '../../../app.state';
import { PartyRequest } from './../../../core/models/requests/party.request.model';

@Component({
  selector: 'app-party-detail',
  templateUrl: './party-detail.component.html',
  styleUrls: ['./party-detail.component.scss']
})
export class PartyDetailComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<Boolean>();

  isNew = true;
  addParent = false;
  party: PartyRequest;
  cities$: Observable<City[]>;
  parents$: Observable<Party[]>;

  constructor(
    route: ActivatedRoute,
    actions$: Actions,
    private store: Store<AppState>,
    private toastr: ToastrService,
    public _location: Location
  ) {
    route.paramMap.subscribe(params => this.loadParty(params.get('id')));
    this.cities$ = store.pipe(
      select(s => s.cities),
      takeUntil(this.destroyed$)
    );
    this.parents$ = store.pipe(
      select(getPartiesExclude(this.party ? this.party.id : null)),
      takeUntil(this.destroyed$)
    );

    this.initActions(actions$);
  }

  ngOnInit() {}
  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  loadParty(partyId) {
    if (partyId === '0') {
      // new party
      this.isNew = true;
      this.party = new PartyRequest();
    } else {
      this.isNew = false;
      this.store
        .pipe(
          select(s => s.parties),
          takeUntil(this.destroyed$),
          switchMap(parties => from(parties)),
          filter(p => p.id === partyId)
        )
        .subscribe(party => this.init(PartyRequest.from(party)));
    }
  }

  init(party: PartyRequest) {
    this.party = party;
    this.addParent = party.parentId != null;
  }

  initActions(actions$: Actions) {
    // FAILURES
    actions$
      .pipe(
        ofType(partyTypes.LOAD_FAIL, partyTypes.ADD_FAIL, partyTypes.UPDATE_FAIL, partyTypes.DELETE_FAIL),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(err => this.toastr.error(err.message, err.error));

    // ADD
    actions$
      .pipe(
        ofType(partyTypes.ADD_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.name + ' was successfully added.', 'Party created');
        this._location.back();
      });

    // UPDATE
    actions$
      .pipe(
        ofType(partyTypes.UPDATE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.name + ' was successfully updated.', 'Party updated');
        this._location.back();
      });

    // DELETE
    actions$
      .pipe(
        ofType(partyTypes.DELETE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.name + ' was successfully removed.', 'Party deleted');
        this._location.back();
      });
  }

  private validate(): Boolean {
    const fdb = this.party.validate();

    if (fdb.length > 0) {
      fdb.forEach(m => this.toastr.error(m, 'Validation error'));
      return false;
    }

    if (this.addParent && (!this.party.parentId || this.party.parentId.length === 0)) {
      this.toastr.error('Please add a parent party or disable the parent.', 'Validation Error');
      return;
    }

    // remove parentId if not wanted
    this.party.parentId = this.addParent ? this.party.parentId : null;

    return true;
  }

  create() {
    if (!this.validate()) {
      return;
    }
    this.store.dispatch(new PartyAddAction(this.party));
  }

  save() {
    if (!this.validate()) {
      return;
    }
    this.store.dispatch(new PartyUpdateAction(this.party));
  }

  delete() {
    if (confirm('Sure to delete this?')) {
      this.store.dispatch(new PartyDeleteAction(this.party));
    }
  }
}
