import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { AppState } from '../../app.state';
import { Store, select } from '@ngrx/store';
import { City } from '@core/models/city.model';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<Boolean>();

  cities$: Observable<City[]>;

  constructor(private store: Store<AppState>) {
    this.cities$ = store.pipe(select(s => s.cities));
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
