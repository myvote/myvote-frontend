import { CityDeleteAction } from './../../../core/store/actions/city.actions';
import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CityRequest } from '@core/models/requests/city.request.model';
import { CityAddAction, CityUpdateAction } from '@core/store/actions/city.actions';
import { Actions, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { from, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AppState } from '../../../app.state';
import { city as cityTypes } from './../../../core/store/types/city.types';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.scss']
})
export class CityDetailComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<Boolean>();

  city: CityRequest = new CityRequest('', '');
  isNew = true;

  constructor(
    route: ActivatedRoute,
    actions$: Actions,
    private store: Store<AppState>,
    private toastr: ToastrService,
    public _location: Location
  ) {
    route.paramMap.subscribe(p => {
      this.destroyed$.next(true);
      this.init(p.get('id'));
    });

    this.initActions(actions$);
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  init(cityId) {
    if (cityId === '0') {
      // create new city
      this.isNew = true;
      this.city = new CityRequest('0', '');
    } else {
      this.isNew = false;
      this.store
        .pipe(
          select(s => s.cities),
          takeUntil(this.destroyed$),
          switchMap(cities => from(cities)),
          filter(c => c.id === cityId)
        )
        .subscribe(c => (this.city = CityRequest.from(c)));
    }
  }

  /**
   * Subscribe to Success/Fail events
   */
  initActions(actions$: Actions) {
    // FAILURES
    actions$
      .pipe(
        ofType(cityTypes.ADD_FAIL, cityTypes.LOAD_FAIL, cityTypes.UPDATE_FAIL, cityTypes.DELETE_FAIL),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(err => this.toastr.error(err.message, err.error));

    // ADD
    actions$
      .pipe(
        ofType(cityTypes.ADD_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(city => {
        this.toastr.success(city.name + ' was successfully added.', 'City added');
        this._location.back();
      });

    // UPDATE
    actions$
      .pipe(
        ofType(cityTypes.UPDATE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(city => {
        this.toastr.success(city.name + ' was successfully updated.', 'City updated');
        this._location.back();
      });

    // DELETE
    actions$
      .pipe(
        ofType(cityTypes.DELETE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(city => {
        this.toastr.success(city.name + ' was successfully removed.', 'City deleted');
        this._location.back();
      });
  }

  createCity() {
    const fb = this.city.isValid();
    if (fb.length > 0) {
      fb.forEach(m => this.toastr.error(m, 'Form Validation'));
      return;
    }

    this.store.dispatch(new CityAddAction(this.city));
  }

  saveCity() {
    const fb = this.city.isValid();
    if (fb.length > 0) {
      fb.forEach(m => this.toastr.error(m, 'Form Validation'));
      return;
    }

    this.store.dispatch(new CityUpdateAction(this.city));
  }

  delete() {
    if (confirm(`Are you sure to delete ${this.city.name}`)) {
      this.store.dispatch(new CityDeleteAction(this.city));
    }
  }
}
