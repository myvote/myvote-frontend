import { ToastrService } from 'ngx-toastr';
import { getPartiesExclude } from './../../../core/store/reducers/party.reducer';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserRequest } from '@core/models/requests/user.request.model';
import { Location } from '@angular/common';
import { AppState } from '../../../app.state';
import { Store, select } from '@ngrx/store';
import { Party } from '@core/models/party.model';
import { Observable, Subject } from 'rxjs';
import { takeUntil, tap, map } from 'rxjs/operators';
import { getUserById } from '@core/store/reducers/user.reducer';
import { UserAddAction, UserDeleteAction, UserUpdateAction } from '@core/store/actions/user.actions';
import { Actions, ofType } from '@ngrx/effects';
import { user as userTypes } from '@core/store/types/user.types';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<boolean>();

  isNew = true;
  hasParty = false;
  user: UserRequest = new UserRequest();
  parties$: Observable<Party[]>;

  constructor(
    public _location: Location,
    route: ActivatedRoute,
    actions: Actions,
    private store: Store<AppState>,
    private toastr: ToastrService
  ) {
    route.paramMap.subscribe(p => this.init(p.get('id')));
    this.subscribeActions(actions);
  }

  ngOnInit() {}
  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  init(id: string) {
    this.destroyed$.next(true);

    this.parties$ = this.store.pipe(
      select(getPartiesExclude(this.user && this.user.partyId ? this.user.partyId : null), takeUntil(this.destroyed$))
    );

    if (id === '0') {
      this.isNew = true;
      this.hasParty = false;
      this.user = new UserRequest();
    } else {
      this.isNew = false;
      this.store
        .pipe(
          select(getUserById(id)),
          takeUntil(this.destroyed$)
        )
        .subscribe(u => {
          this.initUser(UserRequest.from(u));
        });
    }
  }

  initUser(user: UserRequest) {
    this.user = user;
    this.hasParty = user.partyId != null;
  }

  subscribeActions(actions$: Actions) {
    // FAILURES
    actions$
      .pipe(
        ofType(userTypes.LOAD_FAIL, userTypes.ADD_FAIL, userTypes.UPDATE_FAIL, userTypes.DELETE_FAIL),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(err => this.toastr.error(err.message, err.error));

    // ADD
    actions$
      .pipe(
        ofType(userTypes.ADD_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.firstname + ' was successfully added.', 'User created');
        this._location.back();
      });

    // UPDATE
    actions$
      .pipe(
        ofType(userTypes.UPDATE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.firstname + ' was successfully updated.', 'User updated');
        this._location.back();
      });

    // DELETE
    actions$
      .pipe(
        ofType(userTypes.DELETE_SUCCESS),
        takeUntil(this.destroyed$),
        map(action => action['payload'])
      )
      .subscribe(p => {
        this.toastr.success(p.firstname + ' was successfully removed.', 'User deleted');
        this._location.back();
      });
  }

  valid() {
    const fdb = this.user.validate();
    if (fdb.length > 0) {
      fdb.forEach(msg => this.toastr.error(msg, 'Validation Error'));
      return false;
    }

    if (this.hasParty) {
      if (this.user.partyId == null || this.user.partyId.length <= 0) {
        this.toastr.error('Please disable party or fill it in.', 'Validation error');
        return false;
      }
    } else {
      this.user.partyId = null;
    }

    return true;
  }

  create() {
    if (this.valid()) {
      this.store.dispatch(new UserAddAction(this.user));
    }
  }
  save() {
    if (this.valid()) {
      this.store.dispatch(new UserUpdateAction(this.user));
    }
  }
  delete() {
    if (this.valid()) {
      this.store.dispatch(new UserDeleteAction(this.user));
    }
  }
}
