import { UserService } from './../../core/services/user.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { User } from '@core/models/user.model';
import { Store, select } from '@ngrx/store';
import { AppState } from 'app/app.state';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]>;

  constructor(store: Store<AppState>) {
    this.users$ = store.pipe(select(s => s.users));
  }

  ngOnInit() {}
}
