import { Component, OnInit } from '@angular/core';
import { City } from '@core/models/city.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cities$: Observable<Array<City>>;

  constructor() {}

  ngOnInit() {}
}
