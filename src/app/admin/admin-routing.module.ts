import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { UsersComponent } from './users/users.component';
import { CityDetailComponent } from './city/city-detail/city-detail.component';
import { PartyDetailComponent } from './party/party-detail/party-detail.component';
import { HomeComponent } from '@admin/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth.guard';
import { CityComponent } from './city/city.component';
import { PartyComponent } from './party/party.component';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'parties', component: PartyComponent },
      { path: 'parties/:id', component: PartyDetailComponent },
      { path: 'cities', component: CityComponent },
      { path: 'cities/:id', component: CityDetailComponent },
      { path: 'users', component: UsersComponent },
      { path: 'users/:id', component: UserDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
