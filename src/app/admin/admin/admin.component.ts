import { AuthService } from '@core/services/auth.service';
import { CityLoadAction } from './../../core/store/actions/city.actions';
import { PartyLoadAction } from '@core/store/actions/party.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.state';
import { Router } from '@angular/router';
import { UserLoadAction } from '@core/store/actions/user.actions';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  nav_items = [
    { name: 'Home', path: 'home' },
    { name: 'Cities', path: 'cities' },
    { name: 'Parties', path: 'parties' },
    { name: 'Users', path: 'users' }
  ];
  constructor(private store: Store<AppState>, private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.store.dispatch(new PartyLoadAction());
    this.store.dispatch(new CityLoadAction());
    this.store.dispatch(new UserLoadAction());
  }

  logout() {
    this.authService.clear();
    this.router.navigateByUrl('/login?redirectUrl=/admin');
  }
}
