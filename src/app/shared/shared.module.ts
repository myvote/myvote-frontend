import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SharedRoutingModule } from '@shared/shared-routing.module';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  imports: [CommonModule, SharedRoutingModule, FormsModule, NgSelectModule],
  declarations: [LoginComponent, PaginationComponent],
  exports: [PaginationComponent]
})
export class SharedModule {}
