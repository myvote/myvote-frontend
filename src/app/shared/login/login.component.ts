import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserLoginRequest } from '@core/models/requests/user-login.request.model';
import { AuthService } from '@core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = new UserLoginRequest('admin@myvote.be', 'admin');
  private redirect = '/home';

  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    route.queryParams.subscribe(p => (this.redirect = p['redirectUrl']));
  }

  ngOnInit() {}

  validate() {
    const fdb = this.user.validate();
    if (fdb.length > 0) {
      fdb.forEach(m => this.toastr.error(m, 'Validation Error'));
      return false;
    }
    return true;
  }

  login() {
    if (this.validate()) {
      this.authService.login(this.user).subscribe(res => this.onLogin(res.token), err => this.onError(err.error));
    }
  }

  private onLogin(token) {
    console.log(new JwtHelperService().decodeToken(token));
    this.authService.setToken(token);
    this.router.navigateByUrl(this.redirect);
  }

  private onError(error) {
    this.toastr.error(error.message, error.error);
  }
}
