import { TestBed, async, inject } from '@angular/core/testing';

import { PartyGuard } from './party.guard';

describe('PartyGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PartyGuard]
    });
  });

  it('should ...', inject([PartyGuard], (guard: PartyGuard) => {
    expect(guard).toBeTruthy();
  }));
});
