import { PartyGuard } from './party.guard';
import { NgModule } from '@angular/core';
import { PartyComponent } from './party/party.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'party',
    canActivate: [PartyGuard],
    children: [{ path: '', component: PartyComponent }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: []
})
export class PartyRoutingModule {}
