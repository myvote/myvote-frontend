import { PartyRoutingModule } from './party-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartyComponent } from './party/party.component';
import { CoreModule } from '@core/core.module';

@NgModule({
  imports: [CommonModule, PartyRoutingModule, CoreModule],
  declarations: [PartyComponent]
})
export class PartyModule {}
