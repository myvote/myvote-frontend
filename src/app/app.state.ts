import { Party } from '@core/models/party.model';
import { City } from '@core/models/city.model';
import { User } from '@core/models/user.model';

export interface AppState {
  readonly users: User[];
  readonly parties: Party[];
  readonly cities: City[];
}
